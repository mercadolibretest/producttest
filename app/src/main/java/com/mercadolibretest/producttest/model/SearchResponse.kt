package com.mercadolibretest.producttest.model

import com.google.gson.annotations.SerializedName

data class SearchResponse (
    @SerializedName("site_id")
    val site_id: String,
    @SerializedName("results")
    val results: List<Product>
)