package com.mercadolibretest.producttest.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    val id: String,
    val title: String,
    val price: Double,
    val thumbnail: String,
    val condition: String): Parcelable
