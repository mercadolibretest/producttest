package com.mercadolibretest.producttest.navigation

sealed class AppScreens (val route: String) {
    object ProductList: AppScreens("product_list")
    object ProductDetails: AppScreens("product_details")
}
