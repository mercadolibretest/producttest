package com.mercadolibretest.producttest.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mercadolibretest.producttest.screens.ProductDetails
import com.mercadolibretest.producttest.screens.ProductList
import com.mercadolibretest.producttest.viewModel.SharedViewModel

@Composable
fun AppNavigation(sharedViewModel: SharedViewModel) {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = AppScreens.ProductList.route){
            composable(route = AppScreens.ProductList.route){
                ProductList(navController, sharedViewModel)
            }
            composable(route = AppScreens.ProductDetails.route ){
                ProductDetails(navController, sharedViewModel)
            }
    }
}