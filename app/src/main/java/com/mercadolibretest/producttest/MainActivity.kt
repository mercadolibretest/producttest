package com.mercadolibretest.producttest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.ViewModelProvider
import com.mercadolibretest.producttest.navigation.AppNavigation
import com.mercadolibretest.producttest.repository.Repository
import com.mercadolibretest.producttest.ui.theme.ProductTestTheme
import com.mercadolibretest.producttest.viewModel.MainViewModelFactory
import com.mercadolibretest.producttest.viewModel.SharedViewModel


class MainActivity : ComponentActivity() {

    private lateinit var viewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val repository = Repository()
        val viewModelFactory = MainViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SharedViewModel::class.java)

        setContent {
            ProductTestTheme {
                AppNavigation( viewModel)
            }
        }
    }
}