package com.mercadolibretest.producttest.utils

class Constants {
    companion object{
        const val BASE_URL = "https://api.mercadolibre.com"
    }
}