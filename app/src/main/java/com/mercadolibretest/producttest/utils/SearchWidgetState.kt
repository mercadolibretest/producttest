package com.mercadolibretest.producttest.utils

enum class SearchWidgetState {
    OPENED,
    CLOSED
}