package com.mercadolibretest.producttest.api

import com.mercadolibretest.producttest.model.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/sites/MLA/search")
    suspend fun getProductList(@Query("q") text: String): SearchResponse
}