package com.mercadolibretest.producttest.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.mercadolibretest.producttest.R
import com.mercadolibretest.producttest.model.Product
import com.mercadolibretest.producttest.viewModel.SharedViewModel

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ProductDetails(navController: NavController, sharedViewModel: SharedViewModel) {
   Scaffold (topBar = {
       TopAppBar {
           Icon(imageVector = Icons.Default.ArrowBack,
               contentDescription = "Back",
               modifier = Modifier.clickable {
                   navController.popBackStack()
               })
           Spacer(modifier = Modifier.width(8.dp))
           Text(text = "Product Details")
       }
   }) {
       val productItem = sharedViewModel.productItem
       if (productItem != null) {
           BodyContent(navController, productItem)
       }
   }
}

@Composable
fun BodyContent(navController: NavController, productItem: Product) {
    Column(modifier = Modifier.padding(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(modifier = Modifier.fillMaxWidth(),
            text = productItem.title,
            color = MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.body1)
        Spacer(modifier = Modifier.height(20.dp))
        AsyncImage(
            model = productItem.thumbnail,
            contentDescription = null,
            modifier = Modifier.size(350.dp)
        )
        Spacer(modifier = Modifier.height(20.dp))
        Text(modifier = Modifier.fillMaxWidth(),
            text = "$ " + productItem.price,
            color = MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.h5)
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Preview(showSystemUi = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewProductDetails() {
    Scaffold (topBar = {
        TopAppBar {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Back",
                modifier = Modifier.clickable {
                })
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Product Details")
        }
    }) {
        Column(modifier = Modifier.padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Celular Moto G6 32gb Ram 3gb Rom Impecable Envio Gratis",
                    color = MaterialTheme.colors.onBackground,
                    style = MaterialTheme.typography.body1)
                Spacer(modifier = Modifier.height(20.dp))
                Image(
                painterResource(R.drawable.ic_launcher_foreground),
                "imagen",
                modifier = Modifier.size(350.dp))
                Spacer(modifier = Modifier.height(20.dp))
                Text(modifier = Modifier.fillMaxWidth(),
                    text = "$ 4000.00",
                    color = MaterialTheme.colors.onBackground,
                    style = MaterialTheme.typography.h5)
            }
        }
    }
