package com.mercadolibretest.producttest.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.mercadolibretest.producttest.model.Product
import com.mercadolibretest.producttest.navigation.AppScreens
import com.mercadolibretest.producttest.ui.theme.ProductTestTheme
import com.mercadolibretest.producttest.utils.SearchWidgetState
import com.mercadolibretest.producttest.viewModel.SharedViewModel

//TEST DATA
private var products: List<Product> = listOf(
    Product("1","Celular Moto G6 32gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"new"),
    Product("2","Celular Moto G6 16gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"used"),
    Product("3","Celular Moto G6 32gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"new"),
    Product("4","Celular Moto G6 16gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"used"),
    Product("5","Celular Moto G6 32gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"new"),
    Product("6","Celular Moto G6 16gb Ram 3gb Rom Impecable Envio Gratis", 39999.99, "http://http2.mlstatic.com/D_742038-MLA52198876567_102022-O.jpg" ,"used")
)

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ProductList(
    navController: NavController,
    sharedViewModel: SharedViewModel)
{
    val searchWidgetState by sharedViewModel.searchWidgetState
    val searchTextState by sharedViewModel.searchTextState

    Scaffold(
        topBar = {
            MainAppBar(
                searchWidgetState = searchWidgetState,
                searchTextState = searchTextState,
                onTextChange = {
                    sharedViewModel.updateSearchTextState(newValue = it)
                },
                onCloseClicked = {
                    sharedViewModel.updateSearchWidgetState(newValue = SearchWidgetState.CLOSED)
                },
                onSearchClicked = {
                    try {
                        sharedViewModel.getProductList(it)
                    }
                    catch (ex: java.lang.Exception)
                    {
                        Log.d("Error buscando productos", ex.toString())
                    }
                },
                onSearchTriggered = {
                    sharedViewModel.updateSearchWidgetState(newValue = SearchWidgetState.OPENED)
                }
            )
        }
    ) {
        BodyContent(navController, sharedViewModel)
    }
}

@Composable
fun BodyContent(navController: NavController, sharedViewModel: SharedViewModel){
    val productList: List<Product>? = sharedViewModel.productList
    val response = sharedViewModel.response.value
    if (productList != null && productList.isNotEmpty()){
        LazyColumn{
            items(productList) {product ->
                ProductItem(navController, sharedViewModel, product)
            }
        }
    }
    else{
        if(sharedViewModel.firstTime == false && response == false)
        {
            EmptyList("Error en la busqueda o no se encontraron items para ese criterio.")
        }
        else
        {
            EmptyList("Bienvenidos ...")
        }
    }
}

@Composable
fun EmptyList (text: String) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(30.dp))
        Text(
            text = text,
            color= MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.subtitle2)
    }
}

@Composable
fun ProductItem(navController: NavController, sharedViewModel: SharedViewModel, item: Product) {
    Row(modifier = Modifier
        .background(MaterialTheme.colors.background)
        .padding(8.dp)
        .clickable {
            sharedViewModel.updateProduct(item)
            navController.navigate(route = AppScreens.ProductDetails.route)
        }) {
        AsyncImage(
            model = item.thumbnail,
            contentDescription = null,
            modifier = Modifier.size(80.dp)
        )
        Column(modifier = Modifier
            .padding(start = 8.dp)) {
            Text(text = item.title,
                color = MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.body2)
            Text(text = "$ ${item.price}",
                color= MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.subtitle2)
            Spacer(modifier = Modifier
                .height(10.dp))
            Text(text = item.condition,
                color= MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.caption)
        }
    }
}

@Composable
fun MainAppBar(
    searchWidgetState : SearchWidgetState,
    searchTextState : String,
    onTextChange: (String) -> Unit,
    onCloseClicked: () -> Unit,
    onSearchClicked: (String) -> Unit,
    onSearchTriggered: () -> Unit
) {
    when (searchWidgetState) {
        SearchWidgetState.CLOSED -> {
            DefaultAppBar (
                onSearchClicked = onSearchTriggered)
        }
        SearchWidgetState.OPENED -> {
            SearchAppBar(
                text = searchTextState,
                onTextChanged = onTextChange,
                onCloseClicked = onCloseClicked,
                onSearchClicked = onSearchClicked
            )
        }
    }
}

@Composable
fun DefaultAppBar(onSearchClicked: () -> Unit) {
    TopAppBar(
        title = {
            Text(
                text = "ProductList"
            )
        },
        actions = {
            IconButton(onClick = { onSearchClicked() }
            ) {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = "search icon",
                    tint = Color.White
                )
            }
        }
    )
}

@Composable
fun SearchAppBar(
    text: String,
    onTextChanged: (String) -> Unit,
    onCloseClicked: () -> Unit,
    onSearchClicked: (String) -> Unit
){
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp),
        elevation = AppBarDefaults.TopAppBarElevation,
        color = MaterialTheme.colors.primary
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange ={
                onTextChanged(it)
            },
            placeholder ={
                Text(
                    modifier = Modifier.alpha(ContentAlpha.medium),
                    text = "search here..." ,
                    color = Color.White)
            },
            textStyle = TextStyle(
                fontSize = MaterialTheme.typography.subtitle1.fontSize,
                color = Color.White
            ),
            singleLine = true,
            leadingIcon =  {
                IconButton(modifier = Modifier
                    .alpha(ContentAlpha.medium),
                    onClick = {onSearchClicked(text)}
                ) {
                    Icon(
                        imageVector = Icons.Default.Search,
                        contentDescription = "Search Icon",
                        tint = Color.White
                    )
                }
            },
            trailingIcon = {
                IconButton(
                    onClick = {
                         if (text.isNotEmpty()){
                             onTextChanged("")
                         }
                        else{
                            onCloseClicked()
                        }
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.Close,
                        contentDescription = "Close Icon",
                        tint = Color.White
                    )
                }
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    onSearchClicked(text)
                }
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent
            )
        )
    }
}

@Preview(showSystemUi = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun PreviewProductLis() {
    ProductTestTheme(){

        Column(
            horizontalAlignment = Alignment.CenterHorizontally) {
            MainAppBar(
                searchWidgetState = SearchWidgetState.OPENED,
                searchTextState = "",
                onTextChange = {},
                onCloseClicked = {} ,
                onSearchClicked = {},
                onSearchTriggered = {}
            )

            LazyColumn{
                items(products) {product ->
                    Row(modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(8.dp)) {
                        AsyncImage(
                            model = product.thumbnail,
                            contentDescription = null,
                            modifier = Modifier.size(80.dp)
                        )
                        Column(modifier = Modifier
                            .padding(start = 8.dp)) {
                            Text(text = product.title,
                                color = MaterialTheme.colors.onBackground,
                                style = MaterialTheme.typography.body2)
                            Text(text = "$ ${product.price}",
                                color= MaterialTheme.colors.onBackground,
                                style = MaterialTheme.typography.subtitle2)
                            Spacer(modifier = Modifier
                                .height(10.dp))
                            Text(text = product.condition,
                                color= MaterialTheme.colors.onBackground,
                                style = MaterialTheme.typography.caption)
                        }
                    }
                }
            }
        }
    }
}