package com.mercadolibretest.producttest.repository

import com.mercadolibretest.producttest.api.RetrofitInstance
import com.mercadolibretest.producttest.model.SearchResponse

class Repository {

    suspend fun getProductList(text: String): SearchResponse {
        return RetrofitInstance.api.getProductList(text)
    }
}