package com.mercadolibretest.producttest.viewModel

import androidx.compose.runtime.*
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mercadolibretest.producttest.model.Product
import com.mercadolibretest.producttest.repository.Repository
import com.mercadolibretest.producttest.utils.SearchWidgetState
import kotlinx.coroutines.launch

class SharedViewModel(private val repository: Repository): ViewModel()  {
    var productItem by mutableStateOf<Product?>(null)
    private set

    var productList by mutableStateOf<List<Product>?>(null)
    private set

    val response: MutableLiveData<Boolean> = MutableLiveData()


    private val _searchWidgetState: MutableState<SearchWidgetState> = mutableStateOf(value = SearchWidgetState.CLOSED)
    val searchWidgetState: State<SearchWidgetState> = _searchWidgetState

    private val _searchTextState: MutableState<String> = mutableStateOf(value = "")
    val searchTextState : State<String> = _searchTextState

    var firstTime by mutableStateOf<Boolean?>(true)
        private set

    fun updateProduct(product: Product){
        productItem = product
    }

    fun getProductList(text: String) {
        firstTime = false
        viewModelScope.launch {
            val baseResponse = repository.getProductList(text)
            if (baseResponse.results.isNotEmpty()){
                productList = baseResponse.results
                response.value = true
            }
            else{
                response.value = false
                productList = null
            }
        }
    }

    fun updateSearchWidgetState(newValue: SearchWidgetState){
        _searchWidgetState.value = newValue
    }

    fun updateSearchTextState(newValue: String){
        _searchTextState.value = newValue
    }
}